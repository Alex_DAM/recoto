<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property string|null $stock
 * @property string $id
 * @property int|null $precio
 * @property string|null $descripcion
 * @property string|null $tipo
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['precio'], 'integer'],
            [['stock', 'tipo'], 'string', 'max' => 10],
            [['id'], 'string', 'max' => 15],
            [['descripcion'], 'string', 'max' => 500],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'stock' => 'Stock',
            'id' => 'ID',
            'precio' => 'Precio',
            'descripcion' => 'Descripcion',
            'tipo' => 'Tipo',
        ];
    }
}
