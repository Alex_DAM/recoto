<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "motos".
 *
 * @property string|null $modelo
 * @property string $matricula
 * @property string|null $año_produccion
 * @property string|null $marca
 */
class Motos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'motos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matricula'], 'required'],
            [['modelo', 'año_produccion', 'marca'], 'string', 'max' => 15],
            [['matricula'], 'string', 'max' => 8],
            [['matricula'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'modelo' => 'Modelo',
            'matricula' => 'Matricula',
            'año_produccion' => 'Año Produccion',
            'marca' => 'Marca',
        ];
    }
}
