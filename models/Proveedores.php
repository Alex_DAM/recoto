<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores".
 *
 * @property string $cif
 * @property string|null $nombre
 * @property string|null $direccion
 * @property string|null $telefono
 */
class Proveedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cif'], 'required'],
            [['cif', 'telefono'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 20],
            [['direccion'], 'string', 'max' => 50],
            [['cif'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cif' => 'Cif',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
        ];
    }
}
