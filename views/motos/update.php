<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Motos */

$this->title = 'Update Motos: ' . $model->matricula;
$this->params['breadcrumbs'][] = ['label' => 'Motos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->matricula, 'url' => ['view', 'id' => $model->matricula]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="motos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
