<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Motos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="motos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Motos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'modelo',
            'matricula',
            'año_produccion',
            'marca',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
